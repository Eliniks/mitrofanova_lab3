﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GF
{
    class GF
    {

        public string n = null;
            public List<int> N = new List<int>();
        public string result = null;
        private static GF temp1;

        public static object С { get; private set; }

            public GF(string n)// конструктор, который принимает  на вход число в 16-ричной  системе(строку) и переводит его в двоичное ( записывает в список)
            {
                for (int i = 0; i < n.Length; i++)
                {
                    if (n[i] == '0') { N.Add(0); N.Add(0); N.Add(0); N.Add(0); }
                    if (n[i] == '1') { N.Add(0); N.Add(0); N.Add(0); N.Add(1); }
                    if (n[i] == '2') { N.Add(0); N.Add(0); N.Add(1); N.Add(0); }
                    if (n[i] == '3') { N.Add(0); N.Add(0); N.Add(1); N.Add(1); }
                    if (n[i] == '4') { N.Add(0); N.Add(1); N.Add(0); N.Add(0); }
                    if (n[i] == '5') { N.Add(0); N.Add(1); N.Add(0); N.Add(1); }
                    if (n[i] == '6') { N.Add(0); N.Add(1); N.Add(1); N.Add(0); }
                    if (n[i] == '7') { N.Add(0); N.Add(1); N.Add(1); N.Add(1); }
                    if (n[i] == '8') { N.Add(1); N.Add(0); N.Add(0); N.Add(0); }
                    if (n[i] == '9') { N.Add(1); N.Add(0); N.Add(0); N.Add(1); }
                    if (n[i] == 'A') { N.Add(1); N.Add(0); N.Add(1); N.Add(0); }
                    if (n[i] == 'B') { N.Add(1); N.Add(0); N.Add(1); N.Add(1); }
                    if (n[i] == 'C') { N.Add(1); N.Add(1); N.Add(0); N.Add(0); }
                    if (n[i] == 'D') { N.Add(1); N.Add(1); N.Add(0); N.Add(1); }
                    if (n[i] == 'E') { N.Add(1); N.Add(1); N.Add(1); N.Add(0); }
                    if (n[i] == 'F') { N.Add(1); N.Add(1); N.Add(1); N.Add(1); }
                }
            }

            public GF()
            {
            }

            public void Show()
            {
                for (int i = 0; i < N.Count; i++)

                Console.Write(N[i]);
                Console.WriteLine();
            }
            static int Size(GF A, GF B)
            {
                int size = 0;
                if (A.N.Count() >= B.N.Count)
                {
                    size = A.N.Count;
                    int dif = A.N.Count() - B.N.Count;
                    for (int i = 0; i < dif; i++)
                    {
                        B.N.Insert(0, 0);
                    }

                }

                if (A.N.Count() < B.N.Count)
                {
                    size = B.N.Count;
                    int dif = B.N.Count() - A.N.Count;
                    for (int i = 0; i < dif; i++)
                    {
                        A.N.Insert(0, 0);
                    }

                }
                return size;
            }
     static GF setGenerator(GF A)
        {
            
            for (int i = 0; i <= 281; i++)
            {
                A.N.Add(0);
            }
            A.N[0] = 1;
            A.N[1] = 1;
            A.N[277] = 1;
            A.N[272] = 1;
            A.N[281] = 1;
            return A;
            
        }

        static GF Add(GF A,GF B)
        {
            GF C = new GF();
            
              for (int i = 0; i <Size(A,B); i++)
            {
                C.N.Add (A.N[i] ^ B.N[i]);
            }


            return C;
        }
        static  GF KillZero(GF A)
        {
            if (A.N.Count == 1) return A;
            else
            {
                while (A.N[0] == 0)
                {
                    A.N.RemoveAt(0);
                    if (A.N.Count == 1) break;
                }
                return A;
            }

           
        }
       static int CompareToGenerator(GF A)
        {
            GF C = new GF();
            setGenerator(C);
            KillZero(A);
            if (A.N.Count>=C.N.Count())
                return 1;
            else
                return -1;
        }
        static int Compare(GF A, GF B)
        {
            Size(A, B);
            int i = 0;
            while (A.N[i] == B.N[i])

                while (A.N[i] == B.N[i])
                {
                    i++;
                    if (i == A.N.Count)
                        return 0;
                }//numbers are equal
            if (A.N[i] > B.N[i])
                return 1;//num1 > num2
            else
                return -1;//num2>num1

        }
        static GF ShiftBitsToHigh(GF A, int a)

        {
            for (int i = 0; i < a; i++)
            {
                A.N.Add(0);
            }
            return A;
        }
        static GF ModGen(GF A)
        {
            GF Generator = new GF();
            setGenerator(Generator);
            int size = A.N.Count;
            GF result = new GF();
            GF temp = new GF();
            result = A;
            if (CompareToGenerator(result) == -1)
                return result;
            else
            {
                while (CompareToGenerator(result) == 1)
                {
                    int size1 = result.N.Count;
                    temp = ShiftBitsToHigh(Generator, size - size1);
                    result = Add(result, temp);
                    result=KillZero(result);
                }
            }
            return result;
        }


        static GF Mul(GF A, GF B)
        {
            int size = Size(A, B);
            GF result = new GF();
            for (int i = 0; i < 2*size; i++)
            {
               result.N.Add(0);
            }
            for (int i = Size(A,B) - 1; i > 0; i--)
            {
                for (int j = 0; j <= size - 1; j++)
                    result.N[i + j] = (result.N[i + j] + A.N[i] * B.N[j]) % 2;
            }
          result.N.Remove(result.N.Count);
          KillZero(result);
            result =ModGen(result);
            do
            {
                result.N.Insert(0, 0);
            }
            while ((result.N.Count % 4) > 0);
            return result;



        }
       static GF Square(GF A)
        {
            GF B = new GF("0");
        
            if (Compare(A, B) == 0)
            {
                KillZero(A);
                return A;
            }
            else
            {
                KillZero(A);
                for (int i = 1; i <= A.N.Count - 1; i += 2)
                {
                    A.N.Insert(0 + i, 0);
                }
                 A = ModGen(A);
                do
                {
                    A.N.Insert(0, 0);
                } while ((A.N.Count % 4) > 0);
                return A;
            }
        }
        static GF Trace(GF A)
        {
            GF trace = new GF("0");
            GF generator = new GF();
            generator=setGenerator(generator);
            for (int i = 0; i <= 281; i++)
            {
                trace = Add(trace, A);
                A = Square(A);

            }
            return trace;
        }
      


        static GF OberPol(GF A)
        {
            GF result = new GF("1");
            GF temp1 = new GF();
            GF temp2 = new GF("1");
            temp1 = Power(A, temp2);
            for (int i = 0; i <= 281 - 2; i++)
            {
                result = Mul(result, temp1);
                temp1 = Square(temp1);
            }
            result = Square(result);
            do
            {
                result.N.Insert(0, 0);
            } while ((result.N.Count % 4) > 0);
            return result;
        }


        static string BinToHex(GF A)
        {
            string result = null;
            if (A.N.Count % 4 == 3)
            {
                A.N.Insert(0, 0);
            }
            if (A.N.Count % 4 == 2)
            {
                for (int i = 0; i < 2; i++)
                {
                    A.N.Insert(0, 0);
                }
            }
            if (A.N.Count % 4 == 1)
            {
                for (int i = 0; i < 3; i++)
                {
                    A.N.Insert(0, 0);
                }
            }

            for (int i = 0; i < A.N.Count; i += 4)
            {
                if (A.N[i] == 0) { if (A.N[i + 1] == 0) { if (A.N[i + 2] == 0) { if (A.N[i + 3] == 0) result += "0"; } } }
                if (A.N[i] == 0) { if (A.N[i + 1] == 0) { if (A.N[i + 2] == 0) { if (A.N[i + 3] == 1) result = result + "1"; } } }
                if (A.N[i] == 0) { if (A.N[i + 1] == 0) { if (A.N[i + 2] == 1) { if (A.N[i + 3] == 0) result = result + "2"; } } }
                if (A.N[i] == 0) { if (A.N[i + 1] == 0) { if (A.N[i + 2] == 1) { if (A.N[i + 3] == 1) result = result + "3"; } } }
                if (A.N[i] == 0) { if (A.N[i + 1] == 1) { if (A.N[i + 2] == 0) { if (A.N[i + 3] == 0) result = result + "4"; } } }
                if (A.N[i] == 0) { if (A.N[i + 1] == 1) { if (A.N[i + 2] == 0) { if (A.N[i + 3] == 1) result = result + "5"; } } }
                if (A.N[i] == 0) { if (A.N[i + 1] == 1) { if (A.N[i + 2] == 1) { if (A.N[i + 3] == 0) result = result + "6"; } } }
                if (A.N[i] == 0) { if (A.N[i + 1] == 1) { if (A.N[i + 2] == 1) { if (A.N[i + 3] == 1) result = result + "7"; } } }
                if (A.N[i] == 1) { if (A.N[i + 1] == 0) { if (A.N[i + 2] == 0) { if (A.N[i + 3] == 0) result = result + "8"; } } }
                if (A.N[i] == 1) { if (A.N[i + 1] == 0) { if (A.N[i + 2] == 0) { if (A.N[i + 3] == 1) result = result + "9"; } } }
                if (A.N[i] == 1) { if (A.N[i + 1] == 0) { if (A.N[i + 2] == 1) { if (A.N[i + 3] == 0) result = result + "A"; } } }
                if (A.N[i] == 1) { if (A.N[i + 1] == 0) { if (A.N[i + 2] == 1) { if (A.N[i + 3] == 1) result = result + "B"; } } }
                if (A.N[i] == 1) { if (A.N[i + 1] == 1) { if (A.N[i + 2] == 0) { if (A.N[i + 3] == 0) result = result + "C"; } } }
                if (A.N[i] == 1) { if (A.N[i + 1] == 1) { if (A.N[i + 2] == 0) { if (A.N[i + 3] == 1) result = result + "D"; } } }
                if (A.N[i] == 1) { if (A.N[i + 1] == 1) { if (A.N[i + 2] == 1) { if (A.N[i + 3] == 0) result = result + "E"; } } }
                if (A.N[i] == 1) { if (A.N[i + 1] == 1) { if (A.N[i + 2] == 1) { if (A.N[i + 3] == 1) result = result + "F"; } } }
            }

            while (result[0] == '0')
            {
                result.Remove(0, 1);
                if (result.Length == 1) break;
            }
            return result;
        }



        static void Main(string[] args)
        {
            GF A = new GF("00FF623F0351227CD20B79BB8E113E5313294544B6760DA3152BF214FB13BFACA2CAC918");
            GF B = new GF("0188B0E0F02E8C4621CDEA5C807B04F38893AAA4F7C24D79F02EA6E8D91EB1ECE9A673CE");
            GF N = new GF("00B1C696DD5A4F129290CB9EDA3D9E4A9CC1E368ED7A73F758E4CE2386C9CBB9D36FBCA3");

            //GF C = Add(A, B);
            //////GF D = new GF("A");
            GF C = new GF();
            C = Mul(A,B);
            C.Show();
         
            //string add = BinToHex(C);
            //Console.WriteLine(" A+B = " + add);




        }
    }
}
